/* - create function that can accept two parameters
- first parameter will be an array of sorted integer e.g `[2,5,7,8,9,12]`
- second parameter will be a number can be integer or decimal
- your function it should determine if it's any average from summation 2 element from array in first parameter that exactly equal with the number on second parameter
- examples:
*/
function averagePair(arr, obj) {
  // find array [i] + array [i] that equal to integer, using loop?

  for (let i = 0; i < arr.length; i++) {
    for (let j = 1; j < arr.length; j = 2 + j) {
      if (arr[i] + arr[j] == obj) {
        return true;
      } else {
        return false;
      }
    }
  }
}

console.log(averagePair([-1, 0, 3, 4, 5, 6], 4.1)); // false
console.log(averagePair([1, 2, 3], 2.5)); // true
console.log(averagePair([1, 3, 3, 5, 6, 7, 10, 12, 19], 8)); // true
